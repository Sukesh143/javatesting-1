package biz.binarystar.testing.InterviewSample1;

/**
* The App class is mainly for debugging purposes. You can use this class to create objects
* and display their values to check the behavior. 
*
* @author  BinaryStar
* @version 1.0
* @since   2016-08-15
*/
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "ATM Machine Application!!!" );
    }
}
